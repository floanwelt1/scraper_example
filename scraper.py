import requests
from bs4 import BeautifulSoup


def scrape_startup_urls():
    base_url = "https://start-green.net"
    appendix = "/netzwerk/?oid=green-startup&page="
    page = 1

    startup_urls_total = []

    while True:
        req_url = base_url + appendix + str(page)
        html = scrape_url(req_url)
        if html:
            print("Scraping page: " + str(page))
            startup_urls_current_page = find_startup_urls(html, base_url)

            is_last_page = len(startup_urls_total) > 0 and startup_urls_current_page[-1] == startup_urls_total[
                -1] or len(
                startup_urls_current_page) == 0
            if is_last_page:
                break
            else:
                startup_urls_total.extend(startup_urls_current_page)
                page += 1

    print("Number of scraped startups: " + str(len(startup_urls_total)))
    return startup_urls_total


def scrape_url(url):
    res = requests.get(url, verify=False)
    html = BeautifulSoup(res.content, 'html.parser')
    return html


def find_startup_urls(html, base_url):
    a_tags = html.find_all('a', class_="relative")
    startup_urls = [a_tag.get('href') for a_tag in a_tags if a_tag]
    return startup_urls
