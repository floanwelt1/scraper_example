# LIST TO SCRAPE

# General
#
# Name
# Address
# Website
# Telephone
# Tags

# Company Information
#
# Company description
# Business model
# Employees
# Status

# Founding
#
# Founding year
# Planned incorporation
# Business phase
# Founders
# Funding amount searching
# Previous funding

# Details
#
# Product description
# Contribution
# Interested in incubation or acceleration?

# Additional information
# Documents - Not implemented
# People
# Youtube Urls



def parse_page_content(html):
    startup = {}

    if html:
        try:
            # Find name
            startup['name'] = find_name(html)

            # Find content block
            content_block = find_content(html)

            # General information
            startup['address'], \
            startup['website'], \
            startup['telephone'], \
            startup['tags'] = find_general_information(content_block)

            # Company information
            startup['company_description'], \
            startup['business_model'], \
            startup['number_employees'], \
            startup['company_status'] = find_company_information(content_block)

            # Founding information
            startup['founding_year'], \
            startup['planned_incorporation'], \
            startup['business_phase'], \
            startup['founders'], \
            startup['funding_amount_searching'], \
            startup['previous_funding'] = find_founding_information(content_block)

            # Detail information
            startup['product_description'], \
            startup['contribution'], \
            startup['incubation_acceleration_interested'] = find_detail_information(content_block)

            # Additional information
            startup['people'], startup['youtube_urls'] = find_additional_information(content_block)

        except Exception as e:
            print(e)
            return None
    return startup


def find_name(html):
    # If the .find method returns None the subsequent methods throw an error
    # In Swift there are optionals ? that return nil if the condition becomes invalid
    # Is there something similar in python? I couldn't find anything besides single-handedly
    # checking every element with if-conditions
    return html.find('div', class_='headline').find('h1').getText().strip()


def find_content(html):
    return html.find('div', class_="container top10").find('div', class_="col-md-8 top17")

def find_general_information(html):
    header_row = html.find('div', class_="row")
    contact_and_tags = header_row.find('div', class_="col-sm-6").find_next_sibling()

    address = find_address(html)
    website, telephone = find_website_and_telephone(contact_and_tags)
    tags = find_tags(contact_and_tags)
    return address, website, telephone, tags


def find_company_information(content_block):
    company_description = find_headline_small_element(content_block, "Beschreibung")
    business_model = find_p_larger_element(content_block, "Geschäftsmodell: ")
    number_employees = find_p_larger_element(content_block, "Mitarbeiter: ")
    company_status = find_p_larger_element(content_block, "Status Kapitalsuche: ")
    return company_description, business_model, number_employees, company_status


def find_founding_information(content_block):
    founding_year = find_p_larger_element(content_block, "Gegründet: ")
    planned_incorporation = find_p_larger_element(content_block, "Gründung vrstl.: ")
    business_phase = find_p_larger_element(content_block, "Unternehmensphase: ")
    founders = find_headline_small_element(content_block, "Gründer")
    funding_amount_searching = find_p_larger_element(content_block, "Höhe der gesuchten Finanzierung: ")
    previous_funding = find_previous_funding_rounds(content_block)
    return founding_year, planned_incorporation, business_phase, founders, funding_amount_searching, previous_funding


def find_detail_information(content_block):
    product_description = find_headline_small_element(content_block, "Unser Produkt")
    contribution = find_headline_small_element(content_block, "Unser Beitrag")
    incubation_acceleration_interested = interested_in_incubation_acceleration(content_block)
    return product_description, contribution, incubation_acceleration_interested


def find_additional_information(content_block):
    people = find_people(content_block)
    youtube_urls = find_youtube_urls(content_block)
    return people, youtube_urls


def find_address(html):
    # Variable assignment in if query possible?
    # --> if address_headline = html.find('h4', text='Adresse') != None:
    address_headline = html.find('h4', text='Adresse')
    if address_headline:
        address_block = address_headline.find_next_sibling().getText().strip()
        return address_block
    return None


def find_website_and_telephone(contact_block):
    website = None
    telephone = None
    url_tags = contact_block.find_all('a')

    for url_tag in url_tags:
        if url_tag.get('href')[0:4] == 'http':
            website = url_tag.get('href')
        elif url_tag.get('href')[0:4] == 'tel:':
            telephone = url_tag.getText()
    return website, telephone


def find_tags(contact_block):
    tag_symbol = contact_block.find('i', class_='fa-bug')
    if tag_symbol:
        tag_container = tag_symbol.parent
        a_tags = tag_container.find_all('a')
        tags = [a_tag.getText() for a_tag in a_tags if a_tag]
        return tags
    return None


def find_headline_small_element(content_block, descriptor):
    headline = content_block.find('h4', text=descriptor)
    if headline:
        tag = headline.find_next_sibling()
        element = tag.getText()
        return element
    return None


def find_p_larger_element(content_block, descriptor):
    p_larger_list = content_block.find_all('p', class_='larger')
    matches = (p for p in p_larger_list if p.find())
    if matches:
        for p_larger in p_larger_list:
            strong_descriptor = p_larger.find('strong', text=descriptor)
            if strong_descriptor is not None:
                headline = strong_descriptor
                container = headline.parent.getText().strip()
                element = container.split(headline.getText())[1].strip()
                return element
    return None


def find_previous_funding_rounds(content_block):
    # Super ugly due to checking every .find() method for returning something useful
    funding_rounds = []
    headline = content_block.find('h4', text="Finanzierungsrunden")
    if headline:
        funding_round = {}
        funding_round_tag = headline.find_next_sibling().find('p')
        if funding_round_tag:
            funding_date_tag = funding_round_tag.find('strong')
            if funding_date_tag:
                funding_date = funding_date_tag.getText()
                funding_round['date'] = funding_date
            funding_round_raw_text = funding_round_tag.getText()
            funding_round_raw_text = funding_round_raw_text.split(funding_date)[1].strip().split("\n")
            number_of_entries = len(funding_round_raw_text)
            if number_of_entries > 0:
                funding_round['type'] = funding_round_raw_text[0].strip()
            if number_of_entries > 2:
                funding_round['number_of_investors'] = funding_round_raw_text[2].strip().split(" ")[0]
            if number_of_entries > 4:
                funding_round['amount'] = funding_round_raw_text[4].strip()
            source_tag = funding_round_tag.find('a')
            if source_tag:
                funding_round['source'] = source_tag.get('href')
            funding_rounds.append(funding_round)
    return funding_rounds


def interested_in_incubation_acceleration(content_block):
    headline = content_block.find('h4', text="Interesse an der Teilnahme an einem Inkubatoren- oder Accelerator-Programm?")
    if headline:
        response = headline.next_sibling.strip()
        return True if response == 'Ja' else False
        if response == 'Ja':
            return True
        elif response == 'Nein':
            return False
    return None


def find_people(content_block):
    headline = content_block.find('h4', text="Personen")
    if headline:
        row = headline.find_next_sibling()
        urls = row.find_all('a')
        people = [url.get('href') for url in urls]
        return people
    return None


def find_youtube_urls(content_block):
    content_widgets = content_block.find_all('div', class_="widget-media")
    youtube_urls = []
    for widget in content_widgets:
        iframe = widget.find('iframe')
        if iframe:
            youtube_urls.append(iframe.get('src'))
    return youtube_urls
